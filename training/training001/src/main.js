import Vue from 'vue'
import VueRouter from "vue-router";
import VueAxios from 'vue-axios'
import axios from 'axios'
import NProgress from 'nprogress';
import VueAwesomeSwiper from 'vue-awesome-swiper';

import App from './App.vue'
import WeatherPage from './page/WeatherPage.vue'

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/nprogress/nprogress.css';

import { store } from './vuex-store/store'

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueAwesomeSwiper)

Vue.config.productionTip = false

const routes = [
  {
    name: 'WeatherPage',
    path: '/WeatherPage',
    component: WeatherPage
  }
];

const router = new VueRouter({ mode: 'history', routes : routes });

router.beforeResolve((to, from, next) => {
  if (to.name) {
    NProgress.start()
  }
  next()
});

router.afterEach(() => {
  NProgress.done()
});

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
