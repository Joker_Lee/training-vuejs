import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        dataUser: [
            {
            "id": 1,
            "email": "nhatlkh@gmail.com",
            "useerName": "NhatLKH",
            "data": [
                    "HA NOI",
                    "HO CHI MINH",
                    "DA LAT"
            ]
            }
        ]
    },
    getters: {
        locationLst: state => state.dataUser.filter(locations => locations.data)
    },
    mutations: {
        ADD_LOCATION(state, location) {
            state.dataUser[0].data.push(location)
        }
    }
})
